# qrcode

#### 介绍
二维码生成

#### 安装
```
composer require okcoder/qrcode
```

#### 使用说明
```
$qrcode = new \OkCoder\qrCode\Main();

# 生成二维码图片
$qrcode->make('http://www.i5920.com');

# 生成二维码文件
$qrcode->save('http://www.i5920.com', './qrcode.png')

```

### 赞助二维码

![](https://gitee.com/uploads/images/2018/0623/112959_9f84f1f7_696921.png "3.png")
![](https://gitee.com/uploads/images/2018/0623/113008_0014aa83_696921.jpeg "4.jpg")